import socket
import ssl
import sys

HOST = "127.0.0.1"
PORT = 3000

def normal_req():
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
        sock.send("gimme".encode('UTF-8'))
        reply = sock.recv(200)
        sock.close()
        return reply
    except ConnectionRefusedError:
        print("Failed to connect to {}:{}".format(HOST, PORT))
        return ""

if __name__ == "__main__":
    res = normal_req()
    print(res)

